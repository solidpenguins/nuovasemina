#pragma once

#ifndef SEED_H
#define SEED_H

#include "Minion.h"

class Seed : public Minion
{
private:
	bool inSpawnState;
	int inSpawnY;
	int iX, iY; // inSpawnState draw Block
	int seedId;
public:
	Seed(int iX, int iY, int iBlockID, int seedId);
	~Seed(void);

	void Update();
	bool updateMinion();

	void Draw(SDL_Renderer* rR, CIMG* iIMG);

	void collisionWithPlayer(bool TOP);

	void setMinionState(int minionState);
};

#endif