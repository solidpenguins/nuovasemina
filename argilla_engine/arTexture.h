/*
* File:   calTexture.h
* Author: lb
*
* Created on 20 maggio 2016, 1.44
*/

#ifndef ARTEXTURE_H
#define ARTEXTURE_H
#include <cstdlib>
#include <string>
#include <SDL2\SDL.h>
#include <SDL2\SDL_image.h>

class arTexture {
public:
	arTexture();
	~arTexture();
	void init(SDL_Renderer* current_renderer);
	bool loadFromFile(std::string path, SDL_Renderer* rR);
	void free();
	void setColor(Uint8 red, Uint8 green, Uint8 blue);
	void setBlendMode(SDL_BlendMode bleding);
	void setAlpha(Uint8 alpha);
	void Draw(SDL_Renderer* rR, int x, int y, SDL_Rect* clip, bool bRotate);
	void render(SDL_Renderer* rR, int x, int y, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);
	int getWidth();
	int getHeight();

private:
	SDL_Texture* mTexture;
	int mWidth;
	int mHeight;
};

#endif /* ARTEXTURE_H */
