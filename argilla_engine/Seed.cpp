#include "Seed.h"
#include "Core.h"

/* ******************************************** */

Seed::Seed(int iX, int iY, int iBlockID, int seedId) {

	this->fXPos = (float)iX;
	this->fYPos = (float)iY;
	this->iBlockID = iBlockID;

	this->iBlockID = 2;
	this->moveSpeed = 2;
	this->inSpawnState = false;
	this->minionState = 1;
	this->minionSpawned = true;
	this->inSpawnY = 32;
	this->moveDirection = false;
	this->collisionOnlyWithPlayer = true;

	this->iX = iX;
	this->iY = iY;

	this->seedId = seedId;
}

Seed::~Seed(void) {

}

/* ******************************************** */

void Seed::Update() {
	if (inSpawnState) {
		if (inSpawnY <= 0) {
			inSpawnState = false;
		} else {
			if (fYPos > -5) {
				inSpawnY -= 2;
				fYPos -= 2;
			} else {
				inSpawnY -= 1;
				fYPos -= 1;
			}
		}
	}
}

bool Seed::updateMinion() {
	return minionSpawned;
}

void Seed::Draw(SDL_Renderer* rR, CIMG* iIMG) {
	iIMG->Draw(rR, (int)fXPos + (int)CCore::getMap()->getXPos(), (int)fYPos + 2, false);
}

/* ******************************************** */

void Seed::collisionWithPlayer(bool TOP) {
	if(!inSpawnState && minionState >= 0) {
		CCore::getMap()->getPlayer()->setSeed(seedId);
		minionState = -1;
	}
}

void Seed::setMinionState(int minionState) { }