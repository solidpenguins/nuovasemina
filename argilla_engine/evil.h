#pragma once

#ifndef EVIL_H
#define EVIL_H

#include "Minion.h"

class Evil : public Minion
{
private:
	// ----- MinionState, 0 = Alive, 1 = Dead, -1 = Destroy
public:
	Evil(int iX, int iY, int iBlockID, bool moveDirection);
	~Evil(void);

	void Update();
	void Draw(SDL_Renderer* rR, CIMG* iIMG);

	void collisionWithPlayer(bool TOP);

	void setMinionState(int minionState);
};


#endif 