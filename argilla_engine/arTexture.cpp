/*
* File:   calTexture.cpp
* Author: lb
*
* Created on 20 maggio 2016, 1.44
*/

#include "arTexture.h"

arTexture::arTexture() {
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

arTexture::~arTexture() {
	free();
}

bool arTexture::loadFromFile(std::string path, SDL_Renderer* rR) {
	free();
	SDL_Texture* newTexture = NULL;
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL) {
		printf("non posso caricare l\'immagine %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
	}
	else {
		SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0XFF));
		newTexture = SDL_CreateTextureFromSurface(rR, loadedSurface);
		if (newTexture == NULL) {
			printf("niente texture \n");
		}
		else {
			mWidth = loadedSurface->w;
			mHeight = loadedSurface->h;
		}
		SDL_FreeSurface(loadedSurface);
	}
	mTexture = newTexture;
	return mTexture != NULL;
}

void arTexture::Draw(SDL_Renderer* rR, int x, int y, SDL_Rect* clip, bool bRotate) {
	if (!bRotate)
		render(rR, x, y, clip);
	else
		render(rR, x, y, clip, 180.0, NULL, SDL_FLIP_VERTICAL);
}

void arTexture::render(SDL_Renderer* rR, int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip) {
	SDL_Rect renderQuad = { x,y,mWidth,mHeight };
	if (clip != NULL) {
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}
	SDL_RenderCopyEx(rR, mTexture, clip, &renderQuad, angle, center, flip);
}

void arTexture::setColor(Uint8 red, Uint8 green, Uint8 blue) {
	SDL_SetTextureColorMod(mTexture, red, green, blue);
}

void arTexture::setBlendMode(SDL_BlendMode blending) {
	SDL_SetTextureBlendMode(mTexture, blending);
}

void arTexture::setAlpha(Uint8 alpha) {
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

int arTexture::getWidth() {
	return mWidth;
}

int arTexture::getHeight() {
	return mHeight;
}

void arTexture::free() {
	if (mTexture != NULL) {
		SDL_DestroyTexture(mTexture);
		mTexture = NULL;
		mWidth = 0;
		mHeight = 0;
	}
}