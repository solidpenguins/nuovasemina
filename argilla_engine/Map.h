#pragma once

#ifndef MAP_H
#define MAP_H

#include "header.h"
#include "IMG.h"
#include "Block.h"
#include "MapLevel.h"
#include "Player.h"
#include "Vector2.h"
#include "LevelText.h"
#include "Goombas.h"
#include "evil.h"
#include "Seed.h"
#include "Event.h"
#include "Points.h"
#include "BlockDebris.h"
#include <vector>

/* ******************************************** */

class Map
{
private:
	float fXPos, fYPos;

	std::vector<Block*> vBlock;
	int iBlockSize; // Size of vBlock

	std::vector<Block*> vMinion;
	int iMinionSize; // Size of vBlock

	std::vector<std::vector<MapLevel*>> lMap;
	int iMapWidth, iMapHeight;

	std::vector<BlockDebris*> lBlockDebris;
	
	std::vector<LevelText*> vLevelText;

	int currentLevelID;
	int iLevelType; // 0, 1, 2;

	int iSpawnPointID;

	bool bMoveMap;

	int iFrameID;
	int iMapTime;
	
	bool inEvent;
	Event* oEvent;

	// ----- PLAYER -----
	Player* oPlayer;

	// ----- MINIONS -----
	std::vector<std::vector<Minion*>> lMinion;
	int iMinionListSize;

	int getListID(int nXPos);

	int getNumOfMinions(); // ----- Ilosc minionow w grze.
	// ----- MINIONS -----

	// ----- POINTS & COIN -----

	std::vector<Points*> lPoints;

	// ----- POINTS & COIN -----
	
	bool drawLines;

	// ---------- Methods ----------
	
	int getStartBlock();
	int getEndBlock();

	// ----- Load -----
	void loadGameData(SDL_Renderer* rR);

	void createMap();

	void checkSpawnPoint();
	int getNumOfSpawnPoints();
	int getSpawnPointXPos(int iID);
	int getSpawnPointYPos(int iID);

	void loadLVL_1_1();

	void loadMinionsLVL_1_1();

	void clearLevelText();
	
	bool bTP; // -- TP LOOP

	void clearMap();
	void clearMinions();
public:
	Map(void);
	Map(SDL_Renderer* rR);
	~Map(void);

	void Update();

	void UpdatePlayer();
	void UpdateMinions();
	void UpdateMinionsCollisions();
	void UpdateBlocks();
	void UpdateMinionBlokcs();

	void Draw(SDL_Renderer* rR);
	void DrawMap(SDL_Renderer* rR);
	void DrawMinions(SDL_Renderer* rR);
	void DrawGameLayout(SDL_Renderer* rR);
	void DrawLines(SDL_Renderer* rR);

	bool Map::blockUse(int nX, int nY, int iBlockID, int POS);

	void moveMap(int iX, int iY);
	void setSpawnPoint();

	void addPoints(int X, int Y, std::string sText, int iW, int iH);
	void addGoombas(int iX, int iY, bool moveDirection);
	void addEvil(int iX, int iY, bool moveDirection);
	void addSeed(int iX, int iY, int seedId);

	void addText(int X, int Y, std::string sText);

	void lockMinions();

	// ********** COLLISION
	Vector2* getBlockID(int nX, int nY);
	int getBlockIDX(int nX);
	int getBlockIDY(int nY);

	// ----- LEFT
	bool checkCollisionLB(int nX, int nY, int nHitBoxY, bool checkVisible);
	bool checkCollisionLT(int nX, int nY, bool checkVisible);
	// ----- CENTER
	bool checkCollisionLC(int nX, int nY, int nHitBoxY, bool checkVisible);
	bool checkCollisionRC(int nX, int nY, int nHitBoxX, int nHitBoxY, bool checkVisible);
	// ----- RIGHT
	bool checkCollisionRB(int nX, int nY, int nHitBoxX, int nHitBoxY, bool checkVisible);
	bool checkCollisionRT(int nX, int nY, int nHitBoxX, bool checkVisible);
	
	bool checkCollision(Vector2* nV, bool checkVisible);

	void checkCollisionOnTopOfTheBlock(int nX, int nY);
	// ********** COLLISION

	void playerDeath(bool animation, bool instantDeath);

	// ----- LOAD
	void resetGameData();
	void loadLVL();
	void setBackgroundColor(SDL_Renderer* rR);
	std::string getLevelName();

	void startLevelAnimation();

	void structGND(int X, int Y, int iWidth, int iHeight);

	void structT(int X, int Y, int iWidth, int iHeight);

	void setBlockID(int X, int Y, int iBlockID);

	// ----- get & set -----
	float getXPos();
	void setXPos(float iYPos);
	float getYPos();
	void setYPos(float iYPos);

	int getLevelType();
	void setLevelType(int iLevelType);

	int getCurrentLevelID();
	void setCurrentLevelID(int currentLevelID);

	int getMapTime();
	void setMapTime(int iMapTime);

	bool getDrawLines();
	void setDrawLines(bool drawLines);

	void setSpawnPointID(int iSpawnPointID);

	int getMapWidth();

	Block* getBlock(int iID);
	Block* getMinionBlock(int iID);
	MapLevel* getMapBlock(int iX, int iY);

	Player* getPlayer();

	bool getMoveMap();
	void setMoveMap(bool bMoveMap);

	Event* getEvent();
	bool getInEvent();
	void setInEvent(bool inEvent);
};

#endif