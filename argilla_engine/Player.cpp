#include "Player.h"
#include "Core.h"
#include "stdlib.h"
#include "time.h"

/* ******************************************** */

Player::Player(SDL_Renderer* rR, float fXPos, float fYPos) {
	this->fXPos = fXPos;
	this->fYPos = fYPos;
	this->playerHeight = 128;
	this->playerWidth = 78;
	this->playerSize = 128;
	this->iNumOfLives = 3;
	this->iSeedID = NoSeed;

	this->iSpriteID = waitSprite;

	this->iFrameID = 0, this->iComboPoints = 1;

	this->nextFallFrameID = 0;

	this->moveDirection = true;
	this->currentMaxMove = 4;
	this->moveSpeed = 0;
	this->bMove = false;
	this->changeMoveDirection = false;
	this->bSquat = false;
	
	this->attackSpeed = 0;
	this->bAttack = false;

	this->iTimePassed = SDL_GetTicks();

	this->jumpState = 0;
	this->startJumpSpeed = 7.65f;
	this->currentFallingSpeed = 2.7f;

	this->iMoveAnimationTime = 0;
	this->iAttackAnimationTime = 0;
	
	srand((unsigned)time(NULL));
	preload(rR);
}

Player::~Player(void) {
	/*for(std::vector<Sprite*>::iterator i = sJack.begin(); i != sJack.end(); i++) {
		delete (*i);
	}*/

	delete tJackLVLUP;
}

bool Player::preload(SDL_Renderer* rR) {
	bool success = true;
	success = success && load(rR, NoSeed, "jack");
	success = success && load(rR, MetalSeed, "jack1");

	//Morto
	jSprite[deathSprite].x = 0;
	jSprite[deathSprite].y = 128;
	jSprite[deathSprite].w = 128;
	jSprite[deathSprite].h = 128;

	//wait
	jSprite[waitSprite].x = 0;
	jSprite[waitSprite].y = 0;
	jSprite[waitSprite].w = 128;
	jSprite[waitSprite].h = 128;

	//walk
	jSprite[walk0Sprite].x = 128;
	jSprite[walk0Sprite].y = 0;
	jSprite[walk0Sprite].w = 128;
	jSprite[walk0Sprite].h = 128;

	jSprite[walk1Sprite].x = 256;
	jSprite[walk1Sprite].y = 0;
	jSprite[walk1Sprite].w = 128;
	jSprite[walk1Sprite].h = 128;

	jSprite[walk2Sprite].x = 384;
	jSprite[walk2Sprite].y = 0;
	jSprite[walk2Sprite].w = 128;
	jSprite[walk2Sprite].h = 128;

	//Jump
	jSprite[jumpSprite].x = 0;
	jSprite[jumpSprite].y = 0;
	jSprite[jumpSprite].w = 128;
	jSprite[jumpSprite].h = 128;

	//change direction
	jSprite[changeDirectionSprite].x = 0;
	jSprite[changeDirectionSprite].y = 0;
	jSprite[changeDirectionSprite].w = 128;
	jSprite[changeDirectionSprite].h = 128;

	//attack 1
	jSprite[attack0Sprite].x = 128;
	jSprite[attack0Sprite].y = 128;
	jSprite[attack0Sprite].w = 128;
	jSprite[attack0Sprite].h = 128;

	//attack 2
	jSprite[attack1Sprite].x = 256;
	jSprite[attack1Sprite].y = 128;
	jSprite[attack1Sprite].w = 128;
	jSprite[attack1Sprite].h = 128;

	//attack 2
	jSprite[attack2Sprite].x = 384;
	jSprite[attack2Sprite].y = 128;
	jSprite[attack2Sprite].w = 128;
	jSprite[attack2Sprite].h = 128;

	//attack 3
	jSprite[attack3Sprite].x = 0;
	jSprite[attack3Sprite].y = 256;
	jSprite[attack3Sprite].w = 128;
	jSprite[attack3Sprite].h = 128;

	//attack 4
	jSprite[attack4Sprite].x = 128;
	jSprite[attack4Sprite].y = 256;
	jSprite[attack4Sprite].w = 128;
	jSprite[attack4Sprite].h = 128;

	return success;
}

bool Player::load(SDL_Renderer* rR, int seedId, std::string fileName) {
	bool success = true;

 	if (!jTexture[seedId].loadFromFile("files/images/" + fileName + ".png", rR)) {
		printf("CANT LOAD ANIMATION");
		success = false;
	}
	else {
		

	}

	return success;
}

/* ******************************************** */

void Player::Update() {
	playerPhysics();
	if(!bAttack)
		movePlayer();
	playerActions();

	if(iFrameID > 0) {
		--iFrameID;
	} else if(iComboPoints > 1) {
		--iComboPoints;
	}
}

void Player::startAttack() {
	stopMove();
	bAttack = true;
	iAttackAnimationTime = SDL_GetTicks();
	iTimePassed = SDL_GetTicks();
	iSpriteID = getSpriteID(attack0Sprite) - 1;
	attackSpeed = 1;
}

void Player::stopAttack() {
	bAttack = false;
	attackSpeed = 0;
}


void Player::playerActions() {
	if (bAttack)
		attackAnimation();
}

void Player::attackAnimation() {
	if (SDL_GetTicks() - 65 + attackSpeed * 4 > iAttackAnimationTime) {
		iAttackAnimationTime = SDL_GetTicks();
		if (getBaseSpriteID() >= attack4Sprite) {
			setJackSpriteID(getSpriteID(waitSprite));
			bAttack = false;
		}
		else {
			++iSpriteID;
			CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cFIREBALL);
		}
	}
}

void Player::playerPhysics() {
	if (jumpState == 1) {
		updateYPos(-(int)currentJumpSpeed);
		currentJumpDistance += (int)currentJumpSpeed;

		currentJumpSpeed *= (currentJumpDistance / jumpDistance > 0.75f ? 0.972f : 0.986f);

		if (currentJumpSpeed < 2.5f) {
			currentJumpSpeed = 2.5f;
		}

		if (!CCFG::keyJumpPressed && currentJumpDistance > playerHeight) {
			jumpDistance = playerHeight / 2;
			currentJumpDistance = 0;
			currentJumpSpeed = 2.5f;
		}

		if (jumpDistance <= currentJumpDistance) {
			jumpState = 2;
		}
	}
	else {
		if (!CCore::getMap()->checkCollisionLB((int)(fXPos - CCore::getMap()->getXPos() + 2), (int)fYPos + 2, getHitBoxY(), true) &&
			!CCore::getMap()->checkCollisionRB((int)(fXPos - CCore::getMap()->getXPos() - 2), (int)fYPos + 2, getHitBoxX(), getHitBoxY(), true)) {

			if (nextFallFrameID > 0) {
				--nextFallFrameID;
			}
			else {
				currentFallingSpeed *= 1.05f;

				if (currentFallingSpeed > startJumpSpeed) {
					currentFallingSpeed = startJumpSpeed;
				}

				updateYPos((int)currentFallingSpeed);
			}


			jumpState = 2;

			setJackSpriteID(getSpriteID(jumpSprite));
		}
		else if (jumpState == 2) {
			resetJump();
		}
		else {
			checkCollisionBot(0, 0);
		}
	}
}

void Player::movePlayer() {
	if (bMove && !changeMoveDirection && (!bSquat)) {
		if (moveSpeed > currentMaxMove) {
			--moveSpeed;
		}
		else if (SDL_GetTicks() - (100 + 35 * moveSpeed) >= iTimePassed && moveSpeed < currentMaxMove) {
			++moveSpeed;
			iTimePassed = SDL_GetTicks();
		}
		else if (moveSpeed == 0) {
			moveSpeed = 1;
		}
	} else {
		if (SDL_GetTicks() - (50 + 15 * (currentMaxMove - moveSpeed) * 6) > iTimePassed && moveSpeed != 0) {
			--moveSpeed;
			iTimePassed = SDL_GetTicks();
			if (jumpState == 0) setJackSpriteID(getSpriteID(changeDirectionSprite));
		}
		
		if (changeMoveDirection && moveSpeed <= 1) {
			moveDirection = newMoveDirection;
			changeMoveDirection = false;
			bMove = true;
		}
	}
	
	if (moveSpeed > 0) {
		if (moveDirection) {
			updateXPos(moveSpeed);
		}
		else {
			updateXPos(-moveSpeed);
		}

		// ----- SPRITE ANIMATION
		moveAnimation();
		// ----- SPRITE ANIMATION
	}
	else if (jumpState == 0) {
		setJackSpriteID(getSpriteID(waitSprite));
		updateXPos(0);
	} else {
		updateXPos(0);
	}

	if(bSquat) {
		setJackSpriteID(getSpriteID(squatSprite));
	}
}

/* ******************************************** */

void Player::moveAnimation() {
	if(SDL_GetTicks() - 65 + moveSpeed * 4 > iMoveAnimationTime) {
		iMoveAnimationTime = SDL_GetTicks();
		if (getBaseSpriteID() >= walk2Sprite) {
			setJackSpriteID(getSpriteID(walk0Sprite));
		}
		else {		
			++iSpriteID;
		}
	}
}

void Player::startMove() {
	iMoveAnimationTime = SDL_GetTicks();
	iTimePassed = SDL_GetTicks();
	moveSpeed = 1;
	bMove = true;
	bAttack = false;
}

void Player::resetMove() {
	--moveSpeed;
	bMove = false;
}

void Player::stopMove() {
	moveSpeed = 0;
	bMove = false;
	changeMoveDirection = false;
	bSquat = false;
	setJackSpriteID(getSpriteID(waitSprite));
}

void Player::startRun() {
	currentMaxMove = maxMove + 2;
}

void Player::resetRun() {
	currentMaxMove = maxMove;
}

/* ******************************************** */

void Player::jump() {
	startJump(2);
}

void Player::startJump(int iH) {
	currentJumpSpeed = startJumpSpeed;
	jumpDistance = playerSize * iH;
	currentJumpDistance = 0;

	setJackSpriteID(getSpriteID(jumpSprite));

	if(iH > 1)
		CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cJUMP);

	jumpState = 1;
}

void Player::resetJump() {
	jumpState = 0;
	jumpDistance = 0;
	currentJumpDistance = 0;
	currentFallingSpeed = 2.7f;
	nextFallFrameID = 0;
}

/* ******************************************** */

void Player::updateXPos(int iN) {
	checkCollisionBot(iN, 0);
	checkCollisionCenter(iN, 0);
	if (iN > 0) {
		if (!CCore::getMap()->checkCollisionRB((int)(fXPos - CCore::getMap()->getXPos() + iN), (int)fYPos - 2, getHitBoxX(), getHitBoxY(), true) && !CCore::getMap()->checkCollisionRT((int)(fXPos - CCore::getMap()->getXPos() + iN), (int)fYPos + 2, getHitBoxX(), true))
		{
			if (fXPos >= 416 && CCore::getMap()->getMoveMap()) {
				CCore::getMap()->moveMap(-iN, 0);
			}
			else {
				fXPos += iN;
			}
		}
		else {
			updateXPos(iN - 1);
			if (moveSpeed > 1 && jumpState == 0) --moveSpeed;
		}
	} else if (iN < 0) {
		if (!CCore::getMap()->checkCollisionLB((int)(fXPos - CCore::getMap()->getXPos() + iN), (int)fYPos - 2, getHitBoxY(), true) && !CCore::getMap()->checkCollisionLT((int)(fXPos - CCore::getMap()->getXPos() + iN), (int)fYPos + 2, true))
			{
			if (fXPos <= 192 && CCore::getMap()->getXPos() && CCore::getMap()->getMoveMap() && CCFG::canMoveBackward) {

				CCore::getMap()->moveMap(-iN, 0);
			}
			else if(fXPos - CCore::getMap()->getXPos() + iN >= 0 && fXPos >= 0) {
				fXPos += iN;
			} else if(CCFG::canMoveBackward && fXPos >= 0) {
				updateXPos(iN + 1);
			}
		}
		else {
			updateXPos(iN + 1);
			if (moveSpeed > 1 && jumpState == 0) --moveSpeed;
		}
	}
}

void Player::updateYPos(int iN) {
	bool bLEFT, bRIGHT;

	if (iN > 0) {
		bLEFT = CCore::getMap()->checkCollisionLB((int)(fXPos - CCore::getMap()->getXPos() + 2), (int)fYPos + iN, getHitBoxY(), true);
		bRIGHT = CCore::getMap()->checkCollisionRB((int)(fXPos - CCore::getMap()->getXPos() - 2), (int)fYPos + iN, getHitBoxX(), getHitBoxY(), true);

		if (!bLEFT && !bRIGHT) {
			fYPos += iN;
		}
		else {
			if (jumpState == 2) {
				jumpState = 0;
			}
			updateYPos(iN - 1);
		}
	}
	else if (iN < 0) {
		bLEFT = CCore::getMap()->checkCollisionLT((int)(fXPos - CCore::getMap()->getXPos() + 2), (int)fYPos + iN, false);
		bRIGHT = CCore::getMap()->checkCollisionRT((int)(fXPos - CCore::getMap()->getXPos() - 2), (int)fYPos + iN, getHitBoxX(), false);

		if (!bLEFT && !bRIGHT) {
			fYPos += iN;
		}
		else {
			if (jumpState == 1) {
				if (!bLEFT && bRIGHT) {
					Vector2* vRT = getBlockRT(fXPos - CCore::getMap()->getXPos(), fYPos + iN);

					if (!CCore::getMap()->getBlock(CCore::getMap()->getMapBlock(vRT->getX(), vRT->getY())->getBlockID())->getVisible()) {
						if (CCore::getMap()->blockUse(vRT->getX(), vRT->getY(), CCore::getMap()->getMapBlock(vRT->getX(), vRT->getY())->getBlockID(), 0)) {
							jumpState = 2;
						}
						else {
							fYPos += iN;
						}
					}
					else if ((int)(fXPos + getHitBoxX() - CCore::getMap()->getXPos()) % playerSize <= 8) {
						updateXPos((int)-((int)(fXPos + getHitBoxX() - CCore::getMap()->getXPos()) % playerSize));
					}
					else if (CCore::getMap()->getBlock(CCore::getMap()->getMapBlock(vRT->getX(), vRT->getY())->getBlockID())->getUse()) {
						if (CCore::getMap()->blockUse(vRT->getX(), vRT->getY(), CCore::getMap()->getMapBlock(vRT->getX(), vRT->getY())->getBlockID(), 0)) {
							jumpState = 2;
						}
						else {
							fYPos += iN;
						}
					}
					else {
						jumpState = 2;
					}

					delete vRT;
				}
				else if (bLEFT && !bRIGHT) {
					Vector2* vLT = getBlockLT(fXPos - CCore::getMap()->getXPos(), fYPos + iN);
					if (!CCore::getMap()->getBlock(CCore::getMap()->getMapBlock(vLT->getX(), vLT->getY())->getBlockID())->getVisible()) {
						if (CCore::getMap()->blockUse(vLT->getX(), vLT->getY(), CCore::getMap()->getMapBlock(vLT->getX(), vLT->getY())->getBlockID(), 0)) {
							jumpState = 2;
						}
						else {
							fYPos += iN;
						}
					}
					else if ((int)(fXPos - CCore::getMap()->getXPos()) % playerSize >= 24) {
						updateXPos((int)(playerSize - (int)(fXPos - CCore::getMap()->getXPos()) % playerSize));
					}
					else if (CCore::getMap()->getBlock(CCore::getMap()->getMapBlock(vLT->getX(), vLT->getY())->getBlockID())->getUse()) {
						if (CCore::getMap()->blockUse(vLT->getX(), vLT->getY(), CCore::getMap()->getMapBlock(vLT->getX(), vLT->getY())->getBlockID(), 0)) {
							jumpState = 2;
						}
						else {
							fYPos += iN;
						}
					}
					else {
						jumpState = 2;
					}

					delete vLT;
				}
				else {
					if ((int)(fXPos + getHitBoxX() - CCore::getMap()->getXPos()) % playerSize > playerSize - (int)(fXPos - CCore::getMap()->getXPos()) % playerSize) {
						Vector2* vRT = getBlockRT(fXPos - CCore::getMap()->getXPos(), fYPos + iN);

						if (CCore::getMap()->getBlock(CCore::getMap()->getMapBlock(vRT->getX(), vRT->getY())->getBlockID())->getUse()) {
							if (CCore::getMap()->blockUse(vRT->getX(), vRT->getY(), CCore::getMap()->getMapBlock(vRT->getX(), vRT->getY())->getBlockID(), 0)) {
								jumpState = 2;
							}
						}
						else {
							jumpState = 2;
						}

						delete vRT;
					}
					else {
						Vector2* vLT = getBlockLT(fXPos - CCore::getMap()->getXPos(), fYPos + iN);

						if (CCore::getMap()->getBlock(CCore::getMap()->getMapBlock(vLT->getX(), vLT->getY())->getBlockID())->getUse()) {
							if (CCore::getMap()->blockUse(vLT->getX(), vLT->getY(), CCore::getMap()->getMapBlock(vLT->getX(), vLT->getY())->getBlockID(), 0)) {
								jumpState = 2;
							}
						}
						else {
							jumpState = 2;
						}

						delete vLT;
					}
				}
			}

			updateYPos(iN + 1);
		}
	}

	if ((int)fYPos % 2 == 1) {
		fYPos += 1;
	}

	if (!CCore::getMap()->getInEvent() && fYPos - getHitBoxY() > CCFG::GAME_HEIGHT) {
		CCore::getMap()->playerDeath(false, true);
		fYPos = -80;
	}
}

/* ******************************************** */

bool Player::checkCollisionBot(int nX, int nY) {
	return true;
}

bool Player::checkCollisionCenter(int nX, int nY) {
	return true;
}

/* ******************************************** */

Vector2* Player::getBlockLB(float nX, float nY) {
	return CCore::getMap()->getBlockID((int)nX + 1, (int)nY + getHitBoxY() + 2);
}

Vector2* Player::getBlockRB(float nX, float nY) {
	return CCore::getMap()->getBlockID((int)nX + getHitBoxX() - 1, (int)nY + getHitBoxY() + 2);
}

Vector2* Player::getBlockLC(float nX, float nY) {
	return CCore::getMap()->getBlockID((int)nX - 1, (int)nY + getHitBoxY()/2);
}

Vector2* Player::getBlockRC(float nX, float nY) {
	return CCore::getMap()->getBlockID((int)nX + getHitBoxX() + 1, (int)nY + getHitBoxY()/2);
}

Vector2* Player::getBlockLT(float nX, float nY) {
	return CCore::getMap()->getBlockID((int)nX + 1, (int)nY);
}

Vector2* Player::getBlockRT(float nX, float nY) {
	return CCore::getMap()->getBlockID((int)nX + getHitBoxX() - 1, (int)nY);
}

/* ******************************************** */

void Player::Draw(SDL_Renderer* rR) {
	SDL_Rect* currentClip = &jSprite[getJackSpriteID()];
	jTexture[iSeedID].Draw(rR, (int)fXPos, (int)fYPos + (CCore::getMap()->getInEvent() ? 0 : 2), currentClip, !moveDirection);
}

/* ******************************************** */


int Player::getBaseSpriteID()
{
	//return iSpriteID - (baseSpriteCount*iSeedID);
	return iSpriteID;
}

int Player::getSpriteID(int baseSpriteID)
{
	//return baseSpriteID + (baseSpriteCount*iSeedID);
	return baseSpriteID;
}

int Player::getJackSpriteID() {
	return iSpriteID;
}

void Player::setJackSpriteID(int iID) {
	this->iSpriteID = iID;
}

int Player::getHitBoxX() {
	return playerWidth;
}

int Player::getHitBoxY() {
	return playerHeight;
}

/* ******************************************** */

void Player::setMoveDirection(bool moveDirection) {
	this->moveDirection = moveDirection;
}

bool Player::getChangeMoveDirection() {
	return changeMoveDirection;
}

void Player::setChangeMoveDirection() {
	this->changeMoveDirection = true;
	this->newMoveDirection = !moveDirection;
}

/* ******************************************** */

int Player::getXPos() {
	return (int)fXPos;
}

void Player::setXPos(float fXPos) {
	this->fXPos = fXPos;
}

int Player::getYPos() {
	return (int)fYPos;
}

int Player::getSeed() {
	return iSeedID;
}

void Player::setSeed(int iSeedID) {
	this->iSeedID = iSeedID;
	switch (this->iSeedID)
	{
		default:
			CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cMUSHROOMMEAT);
			CCore::getMap()->addPoints((int)(fXPos - CCore::getMap()->getXPos() + getHitBoxX() / 2), (int)fYPos + 16, "1000", 8, 16);
			break;
	}

	//CCore::getMap()->addPoints((int)(fXPos - CCore::getMap()->getXPos() + getHitBoxX() / 2), (int)fYPos + 16, "1UP", 10, 14);
	//CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cONEUP);
}

void Player::resetSeed() {
	this->iSeedID = NoSeed;
	this->iSpriteID = waitSprite;
}

int Player::getNumOfLives() {
	return iNumOfLives;
}

void Player::setNumOfLives(int iNumOfLives) {
	this->iNumOfLives = iNumOfLives;
}

int Player::getMoveSpeed() {
	return moveSpeed;
}

int Player::getAttackSpeed() {
	return attackSpeed;
}

int Player::getJumpState() {
	return jumpState;
}

bool Player::getMove() {
	return bMove;
}

bool Player::getMoveDirection() {
	return moveDirection;
}

void Player::setNextFallFrameID(int nextFallFrameID) {
	this->nextFallFrameID = nextFallFrameID;
}

void Player::setCurrentJumpSpeed(float currentJumpSpeed) {
	this->currentJumpSpeed = currentJumpSpeed;
}

void Player::setMoveSpeed(int moveSpeed) {
	this->moveSpeed = moveSpeed;
}

void Player::setAttackSpeed(int attackSpeed) {
	this->attackSpeed = attackSpeed;
}

void Player::setYPos(float fYPos) {
	this->fYPos = fYPos;
}

bool Player::getSquat() {
	return bSquat;
}

void Player::setSquat(bool bSquat) {
	if(bSquat && this->bSquat != bSquat) {
		fYPos += 20;
		this->bSquat = bSquat;
	} else if(this->bSquat != bSquat) {

		this->bSquat = bSquat;
	}
}

unsigned int Player::getScore() {
	return iScore;
}

void Player::setScore(unsigned int iScore) {
	this->iScore = iScore;
}

void Player::addComboPoints() {
	++iComboPoints;
	iFrameID = 40;
}

int Player::getComboPoints() {
	return iComboPoints;
}

CIMG* Player::getJackLVLUP() {
	return tJackLVLUP;
}