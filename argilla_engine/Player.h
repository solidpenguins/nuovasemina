#pragma once

#ifndef PLAYER_H
#define PLAYER_H

#include "Sprite.h"
#include "Vector2.h"
#include <vector>
#include <string>
#include "arTexture.h"
#include <string>

class Player
{
private:
	int playerSize;
	int playerHeight;
	int playerWidth;

	SDL_Rect jSprite[12];
	arTexture jTexture[2];
	bool preload(SDL_Renderer* rR);
	bool load(SDL_Renderer* rR, int seedId, std::string fileName);

	int getBaseSpriteID();
	int getSpriteID(int baseSpriteID);
	
	int iSpriteID;
	unsigned int iMoveAnimationTime;
	unsigned int iAttackAnimationTime;

	CIMG* tJackLVLUP;

	float fXPos, fYPos;
	int iNumOfLives;

	unsigned int iScore;

	int iComboPoints, iFrameID;

	// ----- LVL UP

	int iSeedID;

	int inLevelAnimationFrameID;

	// ----- LVL UP
	// ----- MOVE

	bool moveDirection; // true = LEFT, false = RIGHT
	bool bMove;
	bool changeMoveDirection;
	bool newMoveDirection;

	static const int maxMove = 4;
	int currentMaxMove;
	int moveSpeed;
	unsigned int iTimePassed;

	bool bSquat;
	bool bAttack;

	// ----- MOVE
	// ----- JUMP

	int jumpState;

	float startJumpSpeed;
	float currentJumpSpeed;
	float jumpDistance;
	float currentJumpDistance;

	float currentFallingSpeed;

	// ----- JUMP
	int nextFallFrameID;
	
	// ----- Method
	void movePlayer();

	bool checkCollisionBot(int nX, int nY);
	bool checkCollisionCenter(int nX, int nY);

	Vector2* getBlockLB(float nX, float nY);
	Vector2* getBlockRB(float nX, float nY);

	Vector2* getBlockLC(float nX, float nY);
	Vector2* getBlockRC(float nX, float nY);

	Vector2* getBlockLT(float nX, float nY);
	Vector2* getBlockRT(float nX, float nY);
public:
	Player(SDL_Renderer* rR, float fXPos, float fYPos);
	~Player(void);

	void Draw(SDL_Renderer* rR);
	void Update();

	void playerPhysics();

	void playerActions();
	void attackAnimation();

	void updateXPos(int iN);
	void updateYPos(int iN);
	
	// ----- MOVE
	void moveAnimation();

	void startMove();
	void resetMove();
	void stopMove();
	void setMoveDirection(bool moveDirection);
	bool getChangeMoveDirection();
	void setChangeMoveDirection();

	void startRun();
	void resetRun();

	// ----- MOVE
	// ----- JUMP
	void jump();
	void startJump(int iH);
	void resetJump();
	// ----- JUMP

	void startAttack();
	void stopAttack();
	int attackSpeed;

	void setJackSpriteID(int iID);
	int getJackSpriteID();

	int getHitBoxX();
	int getHitBoxY();

	// ----- get & set -----
	
	int getXPos();
	void setXPos(float fXPos);
	int getYPos();
	void setYPos(float fYPos);

	int getSeed();
	void setSeed(int powerLVL);
	void resetSeed();

	int getNumOfLives();
	void setNumOfLives(int iNumOfLives);
	
	int getMoveSpeed();
	bool getMove();
	bool getMoveDirection();
	void setNextFallFrameID(int nextFallFrameID);
	void setCurrentJumpSpeed(float currentJumpSpeed);
	void setMoveSpeed(int moveSpeed);

	int getAttackSpeed();
	void setAttackSpeed(int attackSpeed);

	int getJumpState();

	bool getSquat();
	void setSquat(bool bSquat);

	CIMG* getJackLVLUP();
	
	unsigned int getScore();
	void setScore(unsigned int iScore);
	void addComboPoints();
	int getComboPoints();

	const int baseSpriteCount = 12;
#pragma region SpriteIDs
	const int deathSprite = 0;

	const int waitSprite = 1;

	const int walk0Sprite = 2;
	const int walk1Sprite = 3;
	const int walk2Sprite = 4;

	const int jumpSprite = 5;
	const int squatSprite = 0;
	const int changeDirectionSprite = 6;

	const int attack0Sprite = 7;
	const int attack1Sprite = 8;
	const int attack2Sprite = 9;
	const int attack3Sprite = 10;
	const int attack4Sprite = 11;
#pragma endregion

	const int seedCount = 12;
#pragma region Seeds
	const int NoSeed = 0;
	const int MetalSeed = 1;
	const int SakuraSeed = 2;
#pragma endregion
};

#endif