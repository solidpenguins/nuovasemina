#include "Map.h"
#include "CFG.h"
#include "math.h"
#include "stdlib.h"
#include "time.h"

/* ******************************************** */

Map::Map(void) {

}

Map::Map(SDL_Renderer* rR) {
	oPlayer = new Player(rR, 84, 368);

	this->currentLevelID = 0;

	this->iMapWidth = 0;
	this->iMapHeight = 0;
	this->iLevelType = 0;

	this->drawLines = false;
	this->fXPos = 0;
	this->fYPos = 0;

	this->inEvent = false;

	this->iSpawnPointID = 0;

	this->bMoveMap = true;

	this->iFrameID = 0;

	this->bTP = false;

	CCFG::getText()->setFont(rR, "font");

	oEvent = new Event();

	srand((unsigned)time(NULL));

	loadGameData(rR);
	loadLVL();
}

Map::~Map(void) {
	for(std::vector<Block*>::iterator i = vBlock.begin(); i != vBlock.end(); i++) {
		delete (*i);
	}

	for(std::vector<Block*>::iterator i = vMinion.begin(); i != vMinion.end(); i++) {
		delete (*i);
	}

	delete oEvent;
}

/* ******************************************** */

void Map::Update() {
	UpdateBlocks();

	UpdateMinionBlokcs();

	UpdateMinions();

	if (!inEvent) {
		UpdatePlayer();

		++iFrameID;
		if (iFrameID > 32) {
			iFrameID = 0;
			if (iMapTime > 0) {
				--iMapTime;
				if (iMapTime == 90) {
					CCFG::getMusic()->StopMusic();
					CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cLOWTIME);
				}
				else if (iMapTime == 86) {
					CCFG::getMusic()->changeMusic(true, true);
				}

				if (iMapTime <= 0) {
					playerDeath(true, true);
				}
			}
		}
	}
	else {
		oEvent->Animation();
	}

	for(unsigned int i = 0; i < lBlockDebris.size(); i++) {
		if(lBlockDebris[i]->getDebrisState() != -1) {
			lBlockDebris[i]->Update();
		} else {
			delete lBlockDebris[i];
			lBlockDebris.erase(lBlockDebris.begin() + i);
		}
	}
	
	for(unsigned int i = 0; i < lPoints.size(); i++) {
		if(!lPoints[i]->getDelete()) {
			lPoints[i]->Update();
		} else {
			delete lPoints[i];
			lPoints.erase(lPoints.begin() + i);
		}
	}
}

void Map::UpdatePlayer() {
	oPlayer->Update();
	checkSpawnPoint();
}

void Map::UpdateMinions() {
	for(int i = 0; i < iMinionListSize; i++) {
		for(int j = 0, jSize = lMinion[i].size(); j < jSize; j++) {
			if(lMinion[i][j]->updateMinion()) {
				lMinion[i][j]->Update();
			}
		}
	}
	
	// ----- UPDATE MINION LIST ID
	for(int i = 0; i < iMinionListSize; i++) {
		for(int j = 0, jSize = lMinion[i].size(); j < jSize; j++) {
			if(lMinion[i][j]->minionSpawned) {
				if(lMinion[i][j]->minionState == -1) {
					delete lMinion[i][j];
					lMinion[i].erase(lMinion[i].begin() + j);
					jSize = lMinion[i].size();
					continue;
				}
				
				if(floor(lMinion[i][j]->fXPos / 160) != i) {
					lMinion[(int)floor((int)lMinion[i][j]->fXPos / 160)].push_back(lMinion[i][j]);
					lMinion[i].erase(lMinion[i].begin() + j);
					jSize = lMinion[i].size();
				}
			}
		}
	}
}

void Map::UpdateMinionsCollisions() {
	// ----- COLLISIONS
	for(int i = 0; i < iMinionListSize; i++) {
		for(unsigned int j = 0; j < lMinion[i].size(); j++) {
			if(!lMinion[i][j]->collisionOnlyWithPlayer /*&& lMinion[i][j]->minionSpawned*/ && lMinion[i][j]->deadTime < 0) {
				// ----- WITH MINIONS IN SAME LIST
				for(unsigned int k = j + 1; k < lMinion[i].size(); k++) {
					if(!lMinion[i][k]->collisionOnlyWithPlayer /*&& lMinion[i][k]->minionSpawned*/ && lMinion[i][k]->deadTime < 0) {
						if(lMinion[i][j]->getXPos() < lMinion[i][k]->getXPos()) {
							if(lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX >= lMinion[i][k]->getXPos() && lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX <= lMinion[i][k]->getXPos() + lMinion[i][k]->iHitBoxX && ((lMinion[i][j]->getYPos() <= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY && lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY >= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY) || (lMinion[i][k]->getYPos() <= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY && lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY >= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY))) {
								if(lMinion[i][j]->killOtherUnits && lMinion[i][j]->moveSpeed > 0 && lMinion[i][k]->minionSpawned) {
									lMinion[i][k]->setMinionState(-2);
									lMinion[i][j]->collisionWithAnotherUnit();
								}

								if(lMinion[i][k]->killOtherUnits && lMinion[i][k]->moveSpeed > 0 && lMinion[i][j]->minionSpawned) {
									lMinion[i][j]->setMinionState(-2);
									lMinion[i][k]->collisionWithAnotherUnit();
								}
							
								if(lMinion[i][j]->getYPos() - 4 <= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY && lMinion[i][j]->getYPos() + 4 >= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY) {
									lMinion[i][k]->onAnotherMinion = true;
								} else if(lMinion[i][k]->getYPos() - 4 <= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY && lMinion[i][k]->getYPos() + 4 >= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY) {
									lMinion[i][j]->onAnotherMinion = true;
								} else {
									lMinion[i][j]->collisionEffect();
									lMinion[i][k]->collisionEffect();
								}
							}
						} else {
							if(lMinion[i][k]->getXPos() + lMinion[i][k]->iHitBoxX >= lMinion[i][j]->getXPos() && lMinion[i][k]->getXPos() + lMinion[i][k]->iHitBoxX <= lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX && ((lMinion[i][j]->getYPos() <= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY && lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY >= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY) || (lMinion[i][k]->getYPos() <= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY && lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY >= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY))) {
								if(lMinion[i][j]->killOtherUnits && lMinion[i][j]->moveSpeed > 0 && lMinion[i][k]->minionSpawned) {
									lMinion[i][k]->setMinionState(-2);
									lMinion[i][j]->collisionWithAnotherUnit();
								}

								if(lMinion[i][k]->killOtherUnits && lMinion[i][k]->moveSpeed > 0 && lMinion[i][j]->minionSpawned) {
									lMinion[i][j]->setMinionState(-2);
									lMinion[i][k]->collisionWithAnotherUnit();
								}

								if(lMinion[i][j]->getYPos() - 4 <= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY && lMinion[i][j]->getYPos() + 4 >= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY) {
									lMinion[i][k]->onAnotherMinion = true;
								} else if(lMinion[i][k]->getYPos() - 4 <= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY && lMinion[i][k]->getYPos() + 4 >= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY) {
									lMinion[i][j]->onAnotherMinion = true;
								} else {
									lMinion[i][j]->collisionEffect();
									lMinion[i][k]->collisionEffect();
								}
							}
						}
					}
				}

				// ----- WITH MINIONS IN OTHER LIST
				if(i + 1 < iMinionListSize) {
					for(unsigned int k = 0; k < lMinion[i + 1].size(); k++) {
						if(!lMinion[i + 1][k]->collisionOnlyWithPlayer /*&& lMinion[i + 1][k]->minionSpawned*/ && lMinion[i + 1][k]->deadTime < 0) {
							if(lMinion[i][j]->getXPos() < lMinion[i + 1][k]->getXPos()) {
								if(lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX >= lMinion[i + 1][k]->getXPos() && lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX <= lMinion[i + 1][k]->getXPos() + lMinion[i + 1][k]->iHitBoxX && ((lMinion[i][j]->getYPos() <= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY && lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY >= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY) || (lMinion[i + 1][k]->getYPos() <= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY && lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY >= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY))) {
									if(lMinion[i][j]->killOtherUnits && lMinion[i][j]->moveSpeed > 0 && lMinion[i + 1][k]->minionSpawned) {
										lMinion[i + 1][k]->setMinionState(-2);
										lMinion[i][j]->collisionWithAnotherUnit();
									}

									if(lMinion[i + 1][k]->killOtherUnits && lMinion[i + 1][k]->moveSpeed > 0 && lMinion[i][j]->minionSpawned) {
										lMinion[i][j]->setMinionState(-2);
										lMinion[i + 1][k]->collisionWithAnotherUnit();
									}
									
									if(lMinion[i][j]->getYPos() - 4 <= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY && lMinion[i][j]->getYPos() + 4 >= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY) {
										lMinion[i + 1][k]->onAnotherMinion = true;
									} else if(lMinion[i + 1][k]->getYPos() - 4 <= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY && lMinion[i + 1][k]->getYPos() + 4 >= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY) {
										lMinion[i][j]->onAnotherMinion = true;
									} else {
										lMinion[i][j]->collisionEffect();
										lMinion[i + 1][k]->collisionEffect();
									}
								}
							} else {
								if(lMinion[i + 1][k]->getXPos() + lMinion[i + 1][k]->iHitBoxX >= lMinion[i][j]->getXPos() && lMinion[i + 1][k]->getXPos() + lMinion[i + 1][k]->iHitBoxX < lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX && ((lMinion[i][j]->getYPos() <= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY && lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY >= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY) || (lMinion[i + 1][k]->getYPos() <= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY && lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY >= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY))) {
									if(lMinion[i][j]->killOtherUnits && lMinion[i][j]->moveSpeed > 0 && lMinion[i + 1][k]->minionSpawned) {
										lMinion[i + 1][k]->setMinionState(-2);
										lMinion[i][j]->collisionWithAnotherUnit();
									}

									if(lMinion[i + 1][k]->killOtherUnits && lMinion[i + 1][k]->moveSpeed > 0 && lMinion[i][j]->minionSpawned) {
										lMinion[i][j]->setMinionState(-2);
										lMinion[i + 1][k]->collisionWithAnotherUnit();
									}
									/*
									if(lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY < lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY) {
										lMinion[i][j]->onAnotherMinion = true;
										continue;
									} else {
										lMinion[i + 1][k]->onAnotherMinion = true;
										continue;
									}*/

									if(lMinion[i][j]->getYPos() - 4 <= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY && lMinion[i][j]->getYPos() + 4 >= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY) {
										lMinion[i + 1][k]->onAnotherMinion = true;
									} else if(lMinion[i + 1][k]->getYPos() - 4 <= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY && lMinion[i + 1][k]->getYPos() + 4 >= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY) {
										lMinion[i][j]->onAnotherMinion = true;
									} else {
										lMinion[i][j]->collisionEffect();
										lMinion[i + 1][k]->collisionEffect();
									}
								}
							}
						}
					}
				}
			}
		}
	}

	if(!inEvent) {
		// ----- COLLISION WITH PLAYER
		for(int i = getListID(-(int)fXPos + oPlayer->getXPos()) - (getListID(-(int)fXPos + oPlayer->getXPos()) > 0 ? 1 : 0), iSize = i + 2; i < iSize; i++) {
			for(unsigned int j = 0, jSize = lMinion[i].size(); j < jSize; j++) {
				if(lMinion[i][j]->deadTime < 0) {
					if((oPlayer->getXPos() - fXPos >= lMinion[i][j]->getXPos() && oPlayer->getXPos() - fXPos <= lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX) || (oPlayer->getXPos() - fXPos + oPlayer->getHitBoxX() >= lMinion[i][j]->getXPos() && oPlayer->getXPos() - fXPos + oPlayer->getHitBoxX() <= lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX)) {
						if(lMinion[i][j]->getYPos() - 2 <= oPlayer->getYPos() + oPlayer->getHitBoxY() && lMinion[i][j]->getYPos() + 16 >= oPlayer->getYPos() + oPlayer->getHitBoxY()) {
							lMinion[i][j]->collisionWithPlayer(true);
						} else if((lMinion[i][j]->getYPos() <= oPlayer->getYPos() + oPlayer->getHitBoxY() && lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY >= oPlayer->getYPos() + oPlayer->getHitBoxY()) || (lMinion[i][j]->getYPos() <= oPlayer->getYPos() && lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY >= oPlayer->getYPos())) {
							lMinion[i][j]->collisionWithPlayer(false);
						}
					}
				}
			}
		}
	}
}

void Map::UpdateBlocks() {
}

void Map::UpdateMinionBlokcs() {
	vMinion[0]->getSprite()->Update();
}

/* ******************************************** */

void Map::Draw(SDL_Renderer* rR) {
	DrawMap(rR);
	
	DrawMinions(rR);

	for(unsigned int i = 0; i < lPoints.size(); i++) {
		lPoints[i]->Draw(rR);
	}

	for(unsigned int i = 0; i < lBlockDebris.size(); i++) {
		lBlockDebris[i]->Draw(rR);
	}

	for(unsigned int i = 0; i < vLevelText.size(); i++) {
		CCFG::getText()->Draw(rR, vLevelText[i]->getText(), vLevelText[i]->getXPos() + (int)fXPos, vLevelText[i]->getYPos());
	}
	/*
	if(drawLines) {
		aa->Draw(rR, (int)fXPos, -16);
		DrawLines(rR);
	}*/
	oPlayer->Draw(rR);

	if(inEvent) {
		oEvent->Draw(rR);
	}

	DrawGameLayout(rR);
}

void Map::DrawMap(SDL_Renderer* rR) {
	for(int i = getStartBlock(), iEnd = getEndBlock(); i < iEnd && i < iMapWidth; i++) {
		for(int j = iMapHeight - 1; j >= 0; j--) {
			if(lMap[i][j]->getBlockID() != 0) {
				vBlock[lMap[i][j]->getBlockID()]->Draw(rR, 32 * i + (int)fXPos, CCFG::GAME_HEIGHT - 32 * j - 16 - lMap[i][j]->updateYPos());
			}
		}
	}
}

void Map::DrawMinions(SDL_Renderer* rR) {
	for(int i = 0; i < iMinionListSize; i++) {
		for(int j = 0, jSize = lMinion[i].size(); j < jSize; j++) {
			lMinion[i][j]->Draw(rR, vMinion[lMinion[i][j]->getBloockID()]->getSprite()->getTexture());
			//CCFG::getText()->DrawWS(rR, std::to_string(i), lMinion[i][j]->getXPos() + (int)fXPos, lMinion[i][j]->getYPos(), 0, 0, 0, 8);
		}
	}
}

void Map::DrawGameLayout(SDL_Renderer* rR) {
	CCFG::getText()->Draw(rR, "JACK", 54, 16);

	if(oPlayer->getScore() < 100) {
		CCFG::getText()->Draw(rR, "00000" + std::to_string(oPlayer->getScore()), 54, 32);
	} else if(oPlayer->getScore() < 1000) {
		CCFG::getText()->Draw(rR, "000" + std::to_string(oPlayer->getScore()), 54, 32);
	} else if(oPlayer->getScore() < 10000) {
		CCFG::getText()->Draw(rR, "00" + std::to_string(oPlayer->getScore()), 54, 32);
	} else if(oPlayer->getScore() < 100000) {
		CCFG::getText()->Draw(rR, "0" + std::to_string(oPlayer->getScore()), 54, 32);
	} else {
		CCFG::getText()->Draw(rR, std::to_string(oPlayer->getScore()), 54, 32);
	}

	CCFG::getText()->Draw(rR, "WORLD", 462, 16);
	CCFG::getText()->Draw(rR, getLevelName(), 480, 32);

	CCFG::getText()->Draw(rR, "y", 286, 32);

	CCFG::getText()->Draw(rR, "TIME", 672, 16);
	if(CCFG::getMM()->getViewID() == CCFG::getMM()->eGame) {
		if(iMapTime > 100) {
			CCFG::getText()->Draw(rR, std::to_string(iMapTime), 680, 32);
		} else if(iMapTime > 10) {
			CCFG::getText()->Draw(rR, "0" + std::to_string(iMapTime), 680, 32);
		} else {
			CCFG::getText()->Draw(rR, "00" + std::to_string(iMapTime), 680, 32);
		}
	}
}

void Map::DrawLines(SDL_Renderer* rR) {
	SDL_SetRenderDrawBlendMode(rR, SDL_BLENDMODE_BLEND); // APLHA ON !
	SDL_SetRenderDrawColor(rR, 255, 255, 255, 128);

	for(int i = 0; i < CCFG::GAME_WIDTH / 32 + 1; i++) {
		SDL_RenderDrawLine(rR, 32 * i - (-(int)fXPos) % 32, 0, 32 * i - (-(int)fXPos) % 32, CCFG::GAME_HEIGHT);
	}

	for(int i = 0; i < CCFG::GAME_HEIGHT / 32 + 1; i++) {
		SDL_RenderDrawLine(rR, 0, 32 * i - 16 + (int)fYPos, CCFG::GAME_WIDTH, 32 * i - 16 + (int)fYPos);
	}

	for(int i = 0; i < CCFG::GAME_WIDTH / 32 + 1; i++) {
		for(int j = 0; j < CCFG::GAME_HEIGHT / 32; j++) {
			CCFG::getText()->Draw(rR, std::to_string(i + (-((int)fXPos + (-(int)fXPos) % 32)) / 32), 32 * i + 16 - (-(int)fXPos) % 32 - CCFG::getText()->getTextWidth(std::to_string(i + (-((int)fXPos + (-(int)fXPos) % 32)) / 32), 8) / 2, CCFG::GAME_HEIGHT - 9 - 32 * j, 8);
			CCFG::getText()->Draw(rR, std::to_string(j), 32 * i + 16 - (-(int)fXPos) % 32 - CCFG::getText()->getTextWidth(std::to_string(j), 8) / 2 + 1, CCFG::GAME_HEIGHT - 32 * j, 8);
		}
	}

	SDL_SetRenderDrawBlendMode(rR, SDL_BLENDMODE_NONE); // APLHA OFF !
}

/* ******************************************** */

void Map::moveMap(int nX, int nY) {
	if (fXPos + nX > 0) {
		oPlayer->updateXPos((int)(nX - fXPos));
		fXPos = 0;
	}
	else {
		this->fXPos += nX;
	}
}

int Map::getStartBlock() {
	return (int)(-fXPos - (-(int)fXPos) % 32) / 32;
}

int Map::getEndBlock() {
	return (int)(-fXPos - (-(int)fXPos) % 32 + CCFG::GAME_WIDTH) / 32 + 2;
}

/* ******************************************** */

/* ******************************************** */
/* ---------------- COLLISION ---------------- */

Vector2* Map::getBlockID(int nX, int nY) {
	return new Vector2((int)(nX < 0 ? 0 : nX) / 32, (int)(nY > CCFG::GAME_HEIGHT - 16 ? 0 : (CCFG::GAME_HEIGHT - 16 - nY + 32) / 32));
}

int Map::getBlockIDX(int nX) {
	return (int)(nX < 0 ? 0 : nX) / 32;
}

int Map::getBlockIDY(int nY) {
	return (int)(nY > CCFG::GAME_HEIGHT - 16 ? 0 : (CCFG::GAME_HEIGHT - 16 - nY + 32) / 32);
}

bool Map::checkCollisionLB(int nX, int nY, int nHitBoxY, bool checkVisible) {
	return checkCollision(getBlockID(nX, nY + nHitBoxY), checkVisible);
}

bool Map::checkCollisionLT(int nX, int nY, bool checkVisible) {
	return checkCollision(getBlockID(nX, nY), checkVisible);
}

bool Map::checkCollisionLC(int nX, int nY, int nHitBoxY, bool checkVisible) {
	return checkCollision(getBlockID(nX, nY + nHitBoxY), checkVisible);
}

bool Map::checkCollisionRC(int nX, int nY, int nHitBoxX, int nHitBoxY, bool checkVisible) {
	return checkCollision(getBlockID(nX + nHitBoxX, nY + nHitBoxY), checkVisible);
}

bool Map::checkCollisionRB(int nX, int nY, int nHitBoxX, int nHitBoxY, bool checkVisible) {
	return checkCollision(getBlockID(nX + nHitBoxX, nY + nHitBoxY), checkVisible);
}

bool Map::checkCollisionRT(int nX, int nY, int nHitBoxX, bool checkVisible) {
	return checkCollision(getBlockID(nX + nHitBoxX, nY), checkVisible);
}

bool Map::checkCollision(Vector2* nV, bool checkVisible) {
	bool output = vBlock[lMap[nV->getX()][nV->getY()]->getBlockID()]->getCollision() && (checkVisible ? vBlock[lMap[nV->getX()][nV->getY()]->getBlockID()]->getVisible() : true);
	delete nV;
	return output;
}

void Map::checkCollisionOnTopOfTheBlock(int nX, int nY) {
	switch(lMap[nX][nY + 1]->getBlockID()) {
		case 29: case 71: case 72: case 73:// COIN
			lMap[nX][nY + 1]->setBlockID(0);
			CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cCOIN);
			return;
			break;
	}

	for(int i = (nX - nX%5)/5, iEnd = i + 3; i < iEnd && i < iMinionListSize; i++) {
		for(unsigned int j = 0; j < lMinion[i].size(); j++) {
			if(!lMinion[i][j]->collisionOnlyWithPlayer && lMinion[i][j]->getMinionState() >= 0 && ((lMinion[i][j]->getXPos() >= nX*32 && lMinion[i][j]->getXPos() <= nX*32 + 32) || (lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX >= nX*32 && lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX <= nX*32 + 32))) {
				if(lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY >= CCFG::GAME_HEIGHT - 24 - nY*32 && lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY <= CCFG::GAME_HEIGHT - nY*32 + 16) {
					lMinion[i][j]->moveDirection = !lMinion[i][j]->moveDirection;
					lMinion[i][j]->setMinionState(-2);
				}
			}
		}
	}
}

/* ---------------- COLLISION ---------------- */
/* ******************************************** */
/* ----------------- MINIONS ----------------- */

int Map::getListID(int nX) {
	return (int)(nX / 160);
}

void Map::addPoints(int X, int Y, std::string sText, int iW, int iH) {
	lPoints.push_back(new Points(X, Y, sText, iW, iH));
}

void Map::addGoombas(int iX, int iY, bool moveDirection) {
	lMinion[getListID(iX)].push_back(new Goombas(iX, iY, 0, moveDirection));
}

void Map::addEvil(int iX, int iY, bool moveDirection) {
	lMinion[getListID(iX)].push_back(new Evil(iX, iY, 3, moveDirection));
}

void Map::addSeed(int iX, int iY, int seedId) {
	lMinion[getListID(iX)].push_back(new Seed(iX, iY, 2, seedId));
}

void Map::lockMinions() {
	for(unsigned int i = 0; i < lMinion.size(); i++) {
		for(unsigned int j = 0; j < lMinion[i].size(); j++) {
			lMinion[i][j]->lockMinion();
		}
	}
}
void Map::addText(int X, int Y, std::string sText) {
	vLevelText.push_back(new LevelText(X, Y, sText));
}

int Map::getNumOfMinions() {
	int iOutput = 0;

	for(int i = 0, size = lMinion.size(); i < size; i++) {
		iOutput += lMinion[i].size();
	}

	return iOutput;
}

/* ----------------- MINIONS ----------------- */
/* ******************************************** */

/* ---------- LOAD GAME DATA ---------- */

void Map::loadGameData(SDL_Renderer* rR) {
	std::vector<std::string> tSprite;
	std::vector<unsigned int> iDelay;
	
	// ----- 0 Transparent -----
	tSprite.push_back("transp");
	iDelay.push_back(0);
	vBlock.push_back(new Block(0, new Sprite(rR, tSprite, iDelay, false), false, true, false, false));
	tSprite.clear();
	iDelay.clear();
	// ----- 1 -----
	tSprite.push_back("gnd_red_1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(1, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();

	// --------------- MINION ---------------

	// ----- 0 -----
	tSprite.push_back("goombas_0");
	iDelay.push_back(200);
	tSprite.push_back("goombas_1");
	iDelay.push_back(200);
	vMinion.push_back(new Block(0, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 1 -----
	tSprite.push_back("goombas_ded");
	iDelay.push_back(0);
	vMinion.push_back(new Block(1, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 2 -----
	tSprite.push_back("seed");
	iDelay.push_back(50);
	vMinion.push_back(new Block(2, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 3 -----
	tSprite.push_back("evil");
	iDelay.push_back(50);
	vMinion.push_back(new Block(3, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	
	iBlockSize = vBlock.size();
	iMinionSize = vMinion.size();
}

/* ******************************************** */

void Map::clearMap() {
	for(int i = 0; i < iMapWidth; i++) {
		for(int j = 0; j < iMapHeight; j++) {
			delete lMap[i][j];
		}
		lMap[i].clear();
	}
	lMap.clear();

	this->iMapWidth = this->iMapHeight = 0;
	
	oEvent->eventTypeID = oEvent->eNormal;

	clearLevelText();
}

void Map::clearMinions() {
	for(int i = 0; i < iMinionListSize; i++) {
		for(int j = 0, jSize = lMinion[i].size(); j < jSize; j++) {
			delete lMinion[i][j];
			jSize = lMinion[i].size();
		}
		lMinion[i].clear();
	}
}

/* ******************************************** */

void Map::setBackgroundColor(SDL_Renderer* rR) {
	switch(iLevelType) {
		case 0: case 2:
			SDL_SetRenderDrawColor(rR, 93, 148, 252, 255);
			break;
		case 1: case 3: case 4:
			SDL_SetRenderDrawColor(rR, 0, 0, 0, 255);
			break;
		default:
			SDL_SetRenderDrawColor(rR, 93, 148, 252, 255);
			break;
	}
}

std::string Map::getLevelName() {
	return "" + std::to_string(1 + currentLevelID/4) + "-" + std::to_string(currentLevelID%4 + 1);
}

void Map::loadMinionsLVL_1_1() {
	clearMinions();

	addEvil(32 * 55, 268, true);
	addEvil(32 * 50, 228, true);

	addEvil(32 * 60, 368, true);
	addGoombas(32 * 65, 368, true);
	addGoombas(32 * 60, 368, true);
	addGoombas(32 * 65, 368, true);
}

void Map::createMap() {
	// ----- MIONION LIST -----

	for(int i = 0; i < iMapWidth; i += 5) {
		std::vector<Minion*> temp;
		lMinion.push_back(temp);
	}

	iMinionListSize = lMinion.size();

	// ----- MIONION LIST -----
	// ----- CREATE MAP -----

	for(int i = 0; i < iMapWidth; i++) {
		std::vector<MapLevel*> temp;
		for(int i = 0; i < iMapHeight; i++) {
			temp.push_back(new MapLevel(0));
		}

		lMap.push_back(temp);
	}

	// ----- CREATE MAP -----

	this->bTP = false;
}

void Map::checkSpawnPoint() {
	if(getNumOfSpawnPoints() > 1) {
		for(int i = iSpawnPointID + 1; i < getNumOfSpawnPoints(); i++) {
			if(getSpawnPointXPos(i) > oPlayer->getXPos() - fXPos && getSpawnPointXPos(i) < oPlayer->getXPos() - fXPos + 128) {
				iSpawnPointID = i;
			}
		}
	}
}

int Map::getNumOfSpawnPoints() {
	switch(currentLevelID) {
		case 0: case 1: case 2: case 4: case 5: case 8: case 9: case 10: case 13: case 14: case 16: case 17: case 18: case 20: case 21: case 22: case 24: case 25: case 26:
			return 2;
	}

	return 1;
}

int Map::getSpawnPointXPos(int iID) {
	switch(currentLevelID) {
		case 0:
			switch(iID) {
				case 0:
					return 84;
				case 1:
					return 82*32;
			}
	}

	return 84;
}

int Map::getSpawnPointYPos(int iID) {
	switch(currentLevelID) {
		case 1:
			switch(iID) {
				case 0:
					return 64;
			}
	}

	return CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();
}

void Map::setSpawnPoint() {
	float X = (float)getSpawnPointXPos(iSpawnPointID);

	if(X > 6*32) {
		fXPos = -(X - 6*32);
		oPlayer->setXPos(6*32);
		oPlayer->setYPos((float)getSpawnPointYPos(iSpawnPointID));
	} else {
		fXPos = 0;
		oPlayer->setXPos(X);
		oPlayer->setYPos((float)getSpawnPointYPos(iSpawnPointID));
	}

	oPlayer->setMoveDirection(true);
}

void Map::resetGameData() {
	this->currentLevelID = 0;
	this->iSpawnPointID = 0;

	oPlayer->setScore(0);
	oPlayer->resetSeed();

	oPlayer->setNumOfLives(3);

	setSpawnPoint();

	loadLVL();
}

void Map::loadLVL() {
	switch(currentLevelID) {
		case 0:
			loadLVL_1_1();
			break;
	}
}

// ---------- LEVELTEXT -----

void Map::clearLevelText() {
	for(unsigned int i = 0; i < vLevelText.size(); i++) {
		delete vLevelText[i];
	}

	vLevelText.clear();
}

void Map::loadLVL_1_1() {
	clearMap();

	this->iMapWidth = 260;
	this->iMapHeight = 25;
	this->iLevelType = 0;
	this->iMapTime = 400;

	// ---------- LOAD LISTS ----------
	createMap();

	// ----- MINIONS
	loadMinionsLVL_1_1();
	// ----- GND -----

	structGND(0, 0, 20, 2);
	addSeed(32 * 8, 0, 1);
	addSeed(32 * 9, iMapHeight * 32, 1);
	structGND(20, 0, 2, 3);
	structGND(22, 0, 2, 4);
	structGND(24, 0, 2, 5);
	structGND(26, 0, 20, 5);
	structGND(46, 0, 20, 2);
	structGND(66, 0, 10, 3);
	structGND(80, 0, 15, 2);

	// ----- GND -----

	this->iLevelType = 0;
}

// ----- POS 0 = TOP, 1 = BOT
bool Map::blockUse(int nX, int nY, int iBlockID, int POS) {
	return true;
}

/* ******************************************** */

void Map::playerDeath(bool animation, bool instantDeath) {
	inEvent = true;

	oEvent->resetData();
	oPlayer->resetJump();
	oPlayer->stopMove();

	oEvent->iDelay = 150;
	oEvent->newCurrentLevel = currentLevelID;

	oEvent->newMoveMap = bMoveMap;

	oEvent->eventTypeID = oEvent->eNormal;

	oPlayer->resetSeed();

	if (animation) {
		oEvent->iSpeed = 4;
		oEvent->newLevelType = iLevelType;

		oPlayer->setYPos(oPlayer->getYPos() + 4.0f);

		oEvent->vOLDDir.push_back(oEvent->eDEATHNOTHING);
		oEvent->vOLDLength.push_back(30);

		oEvent->vOLDDir.push_back(oEvent->eDEATHTOP);
		oEvent->vOLDLength.push_back(64);

		oEvent->vOLDDir.push_back(oEvent->eDEATHBOT);
		oEvent->vOLDLength.push_back(CCFG::GAME_HEIGHT - oPlayer->getYPos() + 128);
	}
	else {
		oEvent->iSpeed = 4;
		oEvent->newLevelType = iLevelType;

		oEvent->vOLDDir.push_back(oEvent->eDEATHTOP);
		oEvent->vOLDLength.push_back(1);
	}

	oEvent->vOLDDir.push_back(oEvent->eNOTHING);
	oEvent->vOLDLength.push_back(64);

	if (oPlayer->getNumOfLives() > 1) {
		oEvent->vOLDDir.push_back(oEvent->eLOADINGMENU);
		oEvent->vOLDLength.push_back(90);

		oPlayer->setNumOfLives(oPlayer->getNumOfLives() - 1);

		CCFG::getMusic()->StopMusic();
		CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cDEATH);
	}
	else {
		oEvent->vOLDDir.push_back(oEvent->eGAMEOVER);
		oEvent->vOLDLength.push_back(90);

		oPlayer->setNumOfLives(oPlayer->getNumOfLives() - 1);

		CCFG::getMusic()->StopMusic();
		CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cDEATH);
	}
}

void Map::startLevelAnimation() {

	switch(currentLevelID) {
		case 0:

			break;
		case 1:
			oEvent->resetData();
			oPlayer->resetJump();
			oPlayer->stopMove();

			oEvent->iSpeed = 2;
			oEvent->newLevelType = 1;

			oEvent->iDelay = 150;
			oEvent->newCurrentLevel = 1;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 96;
			oEvent->newPlayerYPos = 64;
			oEvent->newMoveMap = true;

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(7 * 32 + 4);

			oEvent->vOLDDir.push_back(oEvent->ePLAYPIPERIGHT);
			oEvent->vOLDLength.push_back(1);

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(1 * 32 - 2);

			oEvent->vOLDDir.push_back(oEvent->eNOTHING);
			oEvent->vOLDLength.push_back(75);

			oEvent->reDrawX.push_back(220);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(220);
			oEvent->reDrawY.push_back(3);
			break;
	}
}

/* ******************************************** */

void Map::structGND(int X, int Y, int iWidth, int iHeight) {
	for(int i = 0; i < iWidth; i++) {
		for(int j = 0; j < iHeight; j++) {
			lMap[X + i][Y + j]->setBlockID(iLevelType == 0 || iLevelType == 4 ? 1 : iLevelType == 1 ? 26 : iLevelType == 2 ? 92 : iLevelType == 6 ? 166 : iLevelType == 7 ? 181 : 75);
		}
	}
}

void Map::structT(int X, int Y, int iWidth, int iHeight) {
	for(int i = 0; i < iHeight - 1; i++) {
		for(int j = 1; j < iWidth - 1; j++) {
			lMap[X + j][Y + i]->setBlockID(iLevelType == 3 ? 154 : 70);
		}
	}

	for(int i = 1; i < iWidth - 1; i++) {
		lMap[X + i][Y + iHeight - 1]->setBlockID(iLevelType == 3 ? 152 : 68);
	}

	lMap[X][Y + iHeight - 1]->setBlockID(iLevelType == 3 ? 151 : 67);
	lMap[X + iWidth - 1][Y + iHeight - 1]->setBlockID(iLevelType == 3 ? 153 : 69);
}

/* ******************************************** */

void Map::setBlockID(int X, int Y, int iBlockID) {
	if(X >= 0 && X < iMapWidth) {
		lMap[X][Y]->setBlockID(iBlockID);
	}
}

/* ******************************************** */

Player* Map::getPlayer() {
	return oPlayer;
}

float Map::getXPos() {
	return fXPos;
}

void Map::setXPos(float iXPos) {
	this->fXPos = iXPos;
}

float Map::getYPos() {
	return fYPos;
}

void Map::setYPos(float iYPos) {
	this->fYPos = iYPos;
}

int Map::getLevelType() {
	return iLevelType;
}

void Map::setLevelType(int iLevelType) {
	this->iLevelType = iLevelType;
}

int Map::getCurrentLevelID() {
	return currentLevelID;
}

void Map::setCurrentLevelID(int currentLevelID) {
	if(this->currentLevelID != currentLevelID) {
		this->currentLevelID = currentLevelID;
		oEvent->resetRedraw();
		loadLVL();
		iSpawnPointID = 0;
	}

	this->currentLevelID = currentLevelID;
}

void Map::setSpawnPointID(int iSpawnPointID) {
	this->iSpawnPointID = iSpawnPointID;
}

int Map::getMapTime() {
	return iMapTime;
}

void Map::setMapTime(int iMapTime) {
	this->iMapTime = iMapTime;
}

int Map::getMapWidth() {
	return iMapWidth;
}

bool Map::getMoveMap() {
	return bMoveMap;
}

void Map::setMoveMap(bool bMoveMap) {
	this->bMoveMap = bMoveMap;
}

bool Map::getDrawLines() {
	return drawLines;
}

void Map::setDrawLines(bool drawLines) {
	this->drawLines = drawLines;
}

/* ******************************************** */

Event* Map::getEvent() {
	return oEvent;
}

bool Map::getInEvent() {
	return inEvent;
}

void Map::setInEvent(bool inEvent) {
	this->inEvent = inEvent;
}

/* ******************************************** */

Block* Map::getBlock(int iID) {
	return vBlock[iID];
}

Block* Map::getMinionBlock(int iID) {
	return vMinion[iID];
}

MapLevel* Map::getMapBlock(int iX, int iY) {
	return lMap[iX][iY];
}
