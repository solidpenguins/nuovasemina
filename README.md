# Love is in the Hair
# SolidPenguins

Author: LightySnake & LupoBianco

WWW: https://loveisinthehairgame.blogspot.it
Email: info@solidpenguins.com

first game made in C++.

thanks to: 
http://lazyfoo.net/tutorials/SDL/index.php
and Łukasz Jakowski

## Build Pre-requisites

FreeBSD:
    $ pkg install cmake sdl2 sdl2_image sdl2_mixer

OS X (brew):
    $ brew install cmake sdl2 sdl2_image sdl2_mixer

## Building and running

    $ make build run

    # or

    $ mkdir build
    $ cd build
    $ cmake ..
    $ make
    $ ./LIITH